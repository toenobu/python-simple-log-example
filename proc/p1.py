# -*- coding: utf-8 -*-
"""
    p1
"""

import time, random
import logging, logging.config

LOOP_NUMBER = 32

# logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(name)s %(levelname)s:%(message)s')
logging.config.fileConfig("logging.ini", disable_existing_loggers=False)
logger = logging.getLogger("p1")

# logger.info(__name__)


def loop():

    for x in range(LOOP_NUMBER):
        r = random.uniform(0.1, 0.5)
        time.sleep(r)

        if x % 4 == 0:
            logger.info(f"hello {x}")
        else:
            logger.debug(f"hello {x}")
