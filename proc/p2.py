# -*- coding: utf-8 -*-
"""
    p2
"""

import time, random
import logging, logging.config

LOOP_NUMBER = 16

# logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(name)s %(levelname)s:%(message)s')
logging.config.fileConfig("logging.ini", disable_existing_loggers=False)
logger = logging.getLogger("p2")


def loop():

    for x in range(LOOP_NUMBER):
        r = random.uniform(0.1, 0.3)
        time.sleep(r)

        if x % 2 == 0:
            logger.info(f"bye {x}")
        else:
            logger.debug(f"bye {x}")
