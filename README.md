# python-simple-log-sample

## Short descrption
There are some process. The proocess has their own logging settings.

## Some materials
- [How to collect, customize, and centralize Python logs](https://www.datadoghq.com/ja/blog/python-logging-best-practices/)
- [Python logging](https://zetcode.com/python/logging/)
- [Python: ロギング設定をファイルから読み込むときの注意点](https://blog.amedama.jp/entry/2018/04/12/233903)